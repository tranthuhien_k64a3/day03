<head>
    <meta charset="UTF-8">
    <style>
        form{
          width: 450px;
          margin-top: 200px;
          margin-left: auto;
          margin-right: auto;
          border: solid 2px cornflowerblue;
          padding: 40px 80px 40px 80px;
        }
        div{
          margin: 12px 0px;
          display: flex;
        }
        .left-label{
          background: cornflowerblue;
          padding: 8px 10px;
          border: solid 2px #4e76cc;
          margin-right: 20px;
          width: 30%;
          display: block;
          color: white;
        }
        input{
            width: 60%;
            border: 2px solid #4e76cc;
        }
        .gender-input{
            display:flex; 
            align-items: center
        }
        .gender-radio{
            margin-left: 20px;
            background: #4e76cc;
        }
        select{
            border: solid 2px #4e76cc;
        }
        button{
          background: #5eb12fe3;
          padding: 10px 10px;
          border: solid 2px #4e76cc;
          width: 30%;
          display: block;
          color: white;
          font-family: Arial, Helvetica, sans-serif;
          border-radius: 6px;
          margin-top: 20px;
          margin-left: auto;
          margin-right: auto;
        }
    </style>
</head>

<?php
    $gender=array('Nam', 'Nữ');
    $khoa=array('' => null, 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
?>
    
<html>
    <form>
        <div class="name">
            <label class="left-label">Họ và tên</label>
            <input type="text">
        </div>
        <div class="Gender">
            <label class="left-label">Giới tính</label>
            <div>
            <?php for ($i = 0; $i < count($gender); $i++) { ?>
                <label class="gender-input" for="<?php echo $i; ?>">
                    <input type="radio" class="gender-radio" name="gender"><?php echo $gender[$i]; ?>   
                </label>
            <?php } ?>
            </div>
        </div>
        <div class="Khoa">
            <label class="left-label">Phân khoa</label>
            <select>
                <?php foreach($khoa as $key => $value) {?>
                    <option><?php echo $value; ?></option>
                <?php } ?>
            </select>
        </div>
        <button>Đăng ký</button>
    </form>
</html>
    
